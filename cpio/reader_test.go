package cpio

import (
	"bytes"
	//	"fmt"
	"io"
	"os"
	"testing"
)

func TestReader(t *testing.T) {

	f, err := os.Open("../data/test")
	if err != nil {
		t.Fatalf(err.Error())
	}
	defer f.Close()
	r := NewReader(f)
	buf := bytes.NewBuffer(nil)
	w := NewWriter(buf)

	for {
		h, err := r.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			t.Fatalf(err.Error())
		}

		switch h.Type {
		default:
			err = w.WriteHeader(h)
			if err != nil {
				t.Fatalf(err.Error())
			}
		case TYPE_REG:
			//			fmt.Printf("%+v\n", h)
			err = w.WriteHeader(h)
			if err != nil {
				t.Fatalf(err.Error())
			}
			//			i, err := io.CopyN(w, r, h.Size)
			i, err := io.Copy(w, r)
			if err != nil || i != h.Size {
				t.Fatalf("error writing")
			}
		}

	}
}
